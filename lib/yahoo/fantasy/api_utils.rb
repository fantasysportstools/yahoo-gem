module Yahoo
  module Fantasy
    module APIUtils
      attr_accessor :resources

      def self.define_function_with_options(name)
        define_method("#{name}") do |options = {}|
          option_strings = []
          options.each {|k,v| option_strings.push("#{k}=#{v}")}
          option_strings.unshift('') if option_strings.count > 0
          add_resource("#{name}#{option_strings.join(';')}")
          self
        end
      end

      def self.define_function_with_key(name)
        define_method("#{name}") do |key|
          add_resource("#{name}/#{key}")
          self
        end
      end


      def add_resource(resource)
          @resources = [] if !@resources
          @resources.push(resource)
      end

      def reset_resources
        @resources = []
      end

      def url_from_resources
        raise RuntimeError, 'Must specify at least one resource' if !@resources
        @resources.join("/")
      end
    end
  end
end