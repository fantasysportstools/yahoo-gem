require 'recursive-open-struct'
require 'active_support/core_ext/hash'

module Yahoo
  module Fantasy
    class Request
      def initialize(client)
        raise ArgumentError, 'client cannot be nil' unless client 
        client.login if !client.logged_in?
        @client = client
      end

      def perform_yql_request(query)
        _perform_request(make_yql_endpoint(query), :get)
      end

      def perform_request(url, request_method=@client.method)        
        _perform_request(make_endpoint(url), request_method)
      end

      def _perform_request(url, request_method=@client.method)
        raise ArgumentError, 'Must have url' unless url

        resp = nil
        attempts = 0
        while attempts < @client.attempts do 
          attempts += 1          
          should_repeat = false
          endpoint = url
          resp = @client.access_token.get(endpoint)

          # FIXME: not using this now, so we always break after 1 try
          if !should_repeat
            break
          else
            sleep(0.1)
          end
        end

        ros = RecursiveOpenStruct.new(Hash.from_xml(resp.body), :recurse_over_arrays => true)              
        raise RuntimeError, ros.error.description if !resp.kind_of?(Net::HTTPOK)
        ros
      end

      private 
        def make_yql_endpoint(url)
          "https://query.yahooapis.com/v1/yql?q=#{url_encode url}"
        end

        def make_endpoint(url)
          "#{@client.endpoint}/fantasy/v2/#{url}"
        end      
    end
  end
end
