require_relative '../client'
require_relative 'request'
require_relative 'apiv2/api'
require_relative 'yql/helpers'

module Yahoo
  module Fantasy
    class Client < Yahoo::Client
      include Yahoo::Fantasy::APIV2::API
      include Yahoo::Fantasy::APIUtils
      include Yahoo::Fantasy::YQL::Helpers

      def uid
        resp = user.get
        resp.fantasy_content.users.user.guid
      end

      def try_api(url)
        request = Yahoo::Fantasy::Request.new(self)
        request.perform_request(url)
      end

      def get
        resp = nil
        begin
          request = Yahoo::Fantasy::Request.new(self)
          resp = request.perform_request(url_from_resources)
        ensure
          reset_resources          
        end
        resp
      end


    end
  end
end
