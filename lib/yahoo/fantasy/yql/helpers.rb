
module Yahoo
  module Fantasy
    module YQL
      module Helpers
        def yql_get_teams_stats_for_week(team_ids, week)
          base_query = ["select * from fantasysports.teams.roster.stats where"]
          base_query << "(" + team_ids.map{|id| "team_key='#{id}'"}.join(" or ") + ")"
          base_query << "and"
          base_query << ["week='#{week}'", "stats_type='week'", "stats_week='#{week}'"].join( " and ")
          query = base_query.join(' ')
          request = Yahoo::Fantasy::Request.new(self)
          request.perform_yql_request(query)
        end
      end
    end
  end
end
