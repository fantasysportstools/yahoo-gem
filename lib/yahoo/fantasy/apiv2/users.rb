require_relative "../api_utils"

module Yahoo
  module Fantasy
    module APIV2
      module Users
        include Yahoo::Fantasy::APIUtils
        def user
          add_resource("users;use_login=1")          
          self
        end
      end
    end
  end
end
