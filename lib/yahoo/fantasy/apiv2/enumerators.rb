
module Yahoo
  module Fantasy
    module APIV2
      # Contains a bunch of enumerators for the yahoo api
      # 
      # @author [liuisaac]
      module Enumerators

        def enumerate_player_stats_for_week_and_league(league_key, week, player_ids)
          start = 0
          length = player_ids.length
          loop do
            # Yahoo's response limit is 25
            end_index = length > (start + 24) ? start + 24 : length - 1
            break if start > end_index            
            resp = league(league_key).players(:player_keys=>player_ids[start..end_index].join(",")).stats(:type => "week", :week => week).get
            players = resp.fantasy_content.league.players
            break if !players
            start += players.count.to_i
            players.player.each do |player|
              yield player
            end
          end
        end

        # Enumerate through the players for a league
        # 
        # @param league_key [String] yahoo league key
        # @yield [p] pass in the recursive open struct of a yahoo response player
        def enumerate_players_for_league(league_key)
          start = 0
          loop do
            resp = league(league_key).players(:start => start).get
            players = resp.fantasy_content.league.players        
            break if !players 
            start += players.count.to_i        
            # loop through players
            players.player.each do |player|
              yield player
            end
          end
        end

        # Enumerate through the players for a game
        # 
        # @param league_key [String] yahoo league key
        # @yield [p] pass in the recursive open struct of a yahoo response player
        def enumerate_players_for_game(game_key)
          start = 0
          loop do
            resp = game(game_key).players(:start => start).get
            players = resp.fantasy_content.game.players        
            break if !players 
            start += players.count.to_i        
            # loop through players
            players.player.each do |player|
              yield player
            end
          end
        end

      end
    end
  end
end
