require_relative 'enumerators'
require_relative 'resources'
require_relative 'subresources'
require_relative 'users'

module Yahoo
  module Fantasy
    module APIV2
      module API
        include Yahoo::Fantasy::APIV2::Users
        include Yahoo::Fantasy::APIV2::Resources
        include Yahoo::Fantasy::APIV2::Subresources        
        include Yahoo::Fantasy::APIV2::Enumerators
      end
    end
  end
end
