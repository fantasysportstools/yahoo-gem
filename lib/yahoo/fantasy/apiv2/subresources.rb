require_relative "../api_utils"

module Yahoo
  module Fantasy
    module APIV2
      module Subresources
        include Yahoo::Fantasy::APIUtils
        GAME_SUBRESOURCES = ["metadata", "game_weeks", "stat_categories", "position_types", "roster_positions"]
        LEAGUE_SUBRESOURCES = ["metadata", "settings", "scoreboard", "standings", "draftresults", "transactions"]
        TEAM_SUBRESOURCES = ["metadata", "stats", "standings", "matchups", "draftresults"]        
        PLAYER_SUBRESOURCES = ["metadata", "stats", "ownership", "percent_owned", "draft_analysis"]                
        SUBRESOURCE_ENDPOINTS = GAME_SUBRESOURCES | LEAGUE_SUBRESOURCES | TEAM_SUBRESOURCES
        SUBRESOURCE_ENDPOINTS.each do |subresource|
          Yahoo::Fantasy::APIUtils.define_function_with_options(subresource)                              
        end
      end
    end
  end
end
