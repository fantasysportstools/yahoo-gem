require_relative "../api_utils"

module Yahoo
  module Fantasy
    module APIV2
      module Resources
        include Yahoo::Fantasy::APIUtils
        RESOURCES_WITH_OPTIONS = ['games', 'leagues', 'players', 'teams', 'roster', 'transactions']
        RESOURCES_WITH_OPTIONS.each do |resource|
          Yahoo::Fantasy::APIUtils.define_function_with_options(resource)
        end

        RESOURCES_WITH_KEY = ['game', 'league','player', 'team', 'transaction']        
        RESOURCES_WITH_KEY.each do |resource|
          Yahoo::Fantasy::APIUtils.define_function_with_key(resource)                  
        end
      end
    end
  end
end
