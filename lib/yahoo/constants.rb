module Yahoo::Constants
  extend self

  def nfl_game_key(year)
    {
      2014 => "331",
      2015 => "348",
    }.fetch(year, 'nfl')
  end

end
