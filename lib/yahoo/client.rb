require_relative('configuration')
require_relative('constants')
require_relative('authentication')

module Yahoo
  extend Configuration        

  class Client
    include Yahoo::Authentication
    attr_accessor *Configuration::VALID_CONFIG_KEYS

    def initialize_with_options(options = {})
      # Merge the config values from the module and those passed
      # to the client.
      merged_options = Yahoo.options.merge(options)

      # Copy the merged values to this client and ignore those
      # not part of our configuration
      Configuration::VALID_CONFIG_KEYS.each do |key|
        send("#{key}=", merged_options[key])
      end
    end


    def initialize(options={})
      initialize_with_options(options)      
      yield self if block_given?
    end

  end
end
