require 'oauth'
require 'faraday'

module Yahoo
  module Authentication

    attr_accessor :access_token

    def logged_in?
      @access_token != nil
    end

    def login
      @access_token = get_yahoo_access_token!
    end

    private 
      def get_yahoo_request_token
       login_conn = Faraday.new(:url => 'https://login.yahoo.com/') do |faraday|
          faraday.request  :url_encoded             # form-encode POST params
          # faraday.response :logger                  # log requests to STDOUT
          faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        end
        
        resp = login_conn.post do |req|
          req.url '/WSLogin/V1/get_auth_token'
          req.params['login'] = @username
          req.params['passwd'] = @password
          req.params['oauth_consumer_key'] = @consumer_key
        end

        if resp.status != 200
          error_message = resp.body.split("\n")[1].split("=")[1].gsub("\n","")
          raise RuntimeError, error_message
        end
        resp.body.split("=")[1].gsub("\n","")
      end

      def get_yahoo_access_token!
        request_token = get_yahoo_request_token
        consumer = 
          OAuth::Consumer.new(@consumer_key, @consumer_secret, {
            :authorize_url => "https://api.login.yahoo.com/oauth/v2/request_auth",
            :request_token_url => "https://login.yahoo.com/WSLogin/V1/get_auth_token",
            :access_token_url => "https://api.login.yahoo.com/oauth/v2/get_token",
            :oauth_version => "1.0"
          })
        oauth_req_token = OAuth::RequestToken.new(consumer, request_token)
        token_hash = consumer.token_request(:post, consumer.access_token_url, oauth_req_token)
        OAuth::AccessToken.new(consumer,token_hash["oauth_token"],token_hash["oauth_token_secret"])
      end

  end
end
