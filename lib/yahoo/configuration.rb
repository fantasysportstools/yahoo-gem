module Yahoo
  module Configuration
    VALID_CONNECTION_KEYS = [:endpoint, :user_agent, :method].freeze
    VALID_SECRET_KEYS     = [:username, :password, :consumer_key, :consumer_secret].freeze
    VALID_OPTIONS_KEYS    = [:attempts, :format].freeze
    VALID_CONFIG_KEYS     = VALID_CONNECTION_KEYS + VALID_SECRET_KEYS + VALID_OPTIONS_KEYS
 
    DEFAULT_ENDPOINT    = 'http://fantasysports.yahooapis.com'
    DEFAULT_METHOD      = :get
    DEFAULT_USER_AGENT  = "yahoo-gem".freeze
 
    DEFAULT_USERNAME          = nil
    DEFAULT_PASSWORD          = nil    
    DEFAULT_CONSUMER_KEY      = nil
    DEFAULT_CONSUMER_SECRET   = nil    

    DEFAULT_ATTEMPTS     = 3
    DEFAULT_FORMAT       = :json
    
    attr_accessor *VALID_CONFIG_KEYS 

    # Make sure we have the default values set when we get 'extended'
    def self.extended(base)
      base.reset
    end

    def reset 
      VALID_CONFIG_KEYS.each do |key|
        send("#{key}=", Configuration.const_get('DEFAULT_'+key.to_s.upcase))
      end
    end

    def configure
      yield self
    end

    def options
      Hash[ * VALID_CONFIG_KEYS.map { |key| [key, send(key)] }.flatten ]
    end

  end
end
