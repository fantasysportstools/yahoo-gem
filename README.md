# Yahoo

A gem used to interface with Yahoo's Fantasy Sports API

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'yahoo'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install yahoo

## Requirements

* You must get a consumer_secret and consumer_key to successfully access the apis. You may obtain one by creating an application on Yahoo's developer site here: https://developer.yahoo.com/apps/create/

## Usage

To create a client used to access the API:

```ruby
require 'yahoo'

@client = Yahoo::Fantasy::Client.new do |client|
  client.consumer_key = key # fill in with your own consumer key
  client.consumer_scecret = secret # fill in with your own consumer secret

  client.username = username # username to access api for 
  client.password = password # password to access api for
end


```

Then you may begin to make api calls with the client. The gem mirrors yahoo's fantasy sports api scheme when hitting endpoints, with the same notion of resources/subresources. The full documentation can be found at https://developer.yahoo.com/fantasysports/guide/. You can chain queries until the request type is tagged at the end. For example:

```ruby

# http://fantasysports.yahooapis.com/fantasy/v2/users;use_login=1/games;game_keys=nba/leagues
resp = @client.user.games(:game_keys=>'nba').leagues.get 

# http://fantasysports.yahooapis.com/fantasy/v2/leagues;league_keys=12345,54321;out='teams,settings,standings,scoreboard'
league_keys = ['12345', '54321']
resp = @client.leagues(:league_keys => league_keys.join(","), :out => 'teams,settings,standings,scoreboard').get

```

All results are returned as a [Recursive Open Struct](https://github.com/aetherknight/recursive-open-struct).


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/yahoo. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

