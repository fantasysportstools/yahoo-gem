require 'spec_helper'

describe Yahoo::Fantasy::Client do
  it 'can create a new client with correct api' do
    client = Yahoo::Fantasy::Client.new
    expect(client).not_to be nil
    expect(client.endpoint).to eq("http://fantasysports.yahooapis.com")
  end

end
